<?php
    if(isset($_GET["submit"]) && $_GET["submit"] == 'true') {
        if (isset($_POST["username"]) && isset($_POST["password"])){
            session_start();
            if (!isset($db)) {
                include("../components/database.php");
                $db = new Database();
            }
            $hash = hash('sha256', $_POST["username"].'-'.$_POST["password"]);
            $user = $db->getUserByName($_POST["username"]);
            if (!$user["isActive"]){
                Header("Location: ./login.php?success=false");
                exit();
            }
            if ($user["hash"] == $hash){
                $_SESSION["username"] = $user["username"];
                $db->updateDisplayName();
                if (isset($user["email"])){
                    $email = $user["email"];
                }
                include("../components/gravatar.php");
                $_SESSION["pfp"] = $gravatar;
                $_SESSION["loggedIn"] = true;
                $redirect = "../pages";
                if (isset($_COOKIE["redirect"])){
                    $redirect = $_COOKIE["redirect"];
                    setcookie("redirect", null, 1);
                }
                Header("Location: ".$redirect."?loginSuccess=true");
                exit();
            }
            else{
                session_destroy();
                Header("Location: ./login.php?success=false");
                exit();
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Title -->
        <title>Login - Blog</title>

        <!-- Icon -->
        <link rel="icon" type="img/png" href="../assets/logo.png">

        <!-- Meta -->
        <meta name="author" content="Samuel Gartmann">
        <meta name="description" content="This page was made as a school project in 2020">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="UTF-8">

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="../css/materialize.min.css">
        <link rel="stylesheet" type="text/css" href="../css/north.colorscheme.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- JS to check data -->
        <script src="../scripts/validation.js" type="text/javascript"></script>

        <!-- JS for sidenav and toasts -->
        <script src="../scripts/materialize.min.js" type="text/javascript"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                let elems = document.querySelectorAll('.sidenav');
                M.Sidenav.init(elems);
            });
        </script>
    </head>
    <body class="polar-darken-4">
        <main>
            <div class="row">
                <div class="col s0 m3 hide-on-med-and-down"></div>
                <div class="col s12 m12 l6">
                    <div class="row hide-on-small-and-down"></div>
                    <div class="card polar-darken-3">
                        <div class="card-content">
                            <div class="row">
                                <form class="col s12" method="post" onsubmit="return login(this)" action="./login.php?submit=true">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="username" name="username" type="text" class="grey-text text-lighten-3">
                                            <label for="username">Username</label>
                                            <span class="helper-text" data-error="Username cannot be empty!"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input id="password" name="password" type="password" class="grey-text text-lighten-3">
                                            <label for="password">Password</label>
                                            <span class="helper-text" data-error="Password cannot be empty!"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button class="waves-effect waves-light btn polar-darken-1 right" type="submit"><i class="material-icons left">person</i>Log in</button>
                                        <a class="btn waves-effect waves-light btn polar-darken-1" href="../pages/"><i class="material-icons left">arrow_back</i>Back</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php
            if (isset($_GET["success"]) && $_GET["success"] == 'false'):
        ?>
                <script>
                    M.toast({html: 'Invalid credentials!', classes: 'aurora-red'})
                </script>
        <?php
            endif;
        ?>
    </body>
</html>
