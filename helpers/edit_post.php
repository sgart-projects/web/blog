<?php
    session_start();
    if (!isset($db)) {
        include("../components/database.php");
        $db = new Database();
    }
    if (!isset($_SESSION["loggedIn"]) && !$db->isAdmin($_SESSION["username"])){
        header("Location: ../pages/");
        exit();
    }

    if (isset($_GET["action"], $_GET["id"])) {
        $id = $_GET["id"];
        $action = $_GET["action"];

        if ($action == "publish") {
            $query = "UPDATE post SET archived=0 WHERE post_id=?";
        } else if ($action == "edit") {
            header("Location: ../pages/post_edit_form.php?id=$id&action=$action");
            exit();
        } else if ($action == "archive") {
            $query = "UPDATE post SET archived=1 WHERE post_id=?";
        } else if ($action == "update") {
            if (isset($_POST["title"], $_POST["description"])) {
                if (isset($_GET["title"], $_GET["desc"])) {
                    if ($_GET["title"] == $_POST["title"] && $_GET["desc"] == $_POST["description"]) {
                        header("Location: ../pages/my_posts.php");
                        exit();
                    }
                }
                require("validation.php");
                if (checkTitle() && checkDescription()) {
                    $currentDateTime = date_format(new DateTime(), "y-m-d H:i:s");
                    $params = [htmlentities($_POST["title"]), htmlentities($_POST["description"]), $currentDateTime, $id];
                    $title = htmlentities($_POST["title"]);
                    $description = htmlentities($_POST["description"]);
                    $query = "UPDATE post SET title=?, description=?, last_changed=? WHERE post_id=?";
                    $urlParams = "?edit=success";
                } else {
                    header("Location: ../pages/my_posts.php?edit=failed");
                    exit();
                }
            } else {
                header("Location: ../pages/my_posts.php?edit=failed");
                exit();
            }
        }
        else if ($action == "delete") {
            $query = "DELETE FROM post WHERE post_id=$id";
        }
    }

    if (isset($_GET["action"]) && $_GET["action"] == "create" && isset($_GET["type"]) && $_GET["type"] == "text"){
        $title = htmlentities($_POST["title"]);
        $description = htmlentities($_POST["description"]);
        $query = "INSERT INTO post (user_id, title, description) VALUES (?, ?, ?)";
        $user_id = $db->getUserByName($_SESSION["username"])["userId"];
        $params = [$user_id, htmlentities($_POST["title"]), htmlentities($_POST["description"])];
    }

    if (isset($query)){
        if (!isset($params)){
            $params = [$id];
        }
        if (!isset($urlParams)){
            $urlParams = "";
        }
        $db->write($query, $params);
        header("Location: ../pages/my_posts.php$urlParams");
        exit();
    }
    header("Location: ../pages/my_posts.php");
