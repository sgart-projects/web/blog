<?php
function checkUsername(){
    $valid = true;
    if (strlen($_POST["username"]) === 0 &&
        strlen($_POST["username"]) > 50 &&
        !preg_match('/^[A-z0-9_-]{3,50}$/', $_POST["username"])){
        $valid = false;
    }
    return $valid;
}

function checkFirstName(){
    $valid = true;
    if ($_POST["firstName"] != "" &&
        strlen($_POST["firstName"]) > 50 &&
        strlen($_POST["firstName"]) > 0 &&
        !preg_match('/^[A-Za-z\- äëïöü]{2,50}$/', $_POST["firstName"]))
    {
        $valid = false;
    }
    return $valid;
}

function checkLastName(){
    $valid = true;
    if ($_POST["lastName"] != "" &&
        strlen($_POST["lastName"]) > 50 &&
        strlen($_POST["lastName"]) > 0 &&
        !preg_match('/^[A-Za-z\- äëïöü]{2,50}$/', $_POST["lastName"]))
    {
        $valid = false;
    }
    return $valid;
}

function checkEmail(){
    $valid = true;
    if ($_POST["email"] != "" &&
        strlen($_POST["email"]) > 100 &&
        !preg_match('/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/', $_POST["email"]))
    {
        $valid = false;
    }
    return $valid;
}

function checkTitle(){
    $valid = true;
    if ($_POST["title"] != "" &&
        (strlen($_POST["title"]) < 4 ||  strlen($_POST["title"]) > 20)){
        $valid = false;
    }
    return $valid;
}

function checkDescription(){
    $valid = true;
    if (strlen($_POST["description"]) < 4 ||  strlen($_POST["description"]) > 200){
        $valid = false;
    }
    return $valid;
}
