<?php
    if (!isset($row) || !isset($timestamp)){
        Header("Location: ../pages/");
        exit();
    }
    $editMode = false;
    if (isset($edit) && $edit){
        $editMode = true;
    }
?>

<div class="col s12 m6 l4">
    <div class="card medium polar-darken-3 hoverable">
        <div class="card-image">
            <img src="../assets/posts/<?php echo $row["image_path"] ?>" alt="Post Image" class="materialboxed">
            <?php
                if (isset($row["title"]) && $row["title"] != ""){
                    echo '<span class="card-title">'.$row["title"].'</span>';
                }
            ?>
        </div>
        <div class="card-content white-text">
            <?php
                if ($row["created_at"] != $row["last_changed"]){
                    echo '  <span class="right grey-text">[edited]</span>';
                }
            ?>
            <p><?php echo str_replace("\n", "<br>", $row["description"]) ?></p>
        </div>
        <div class="card-action">
            <?php
                if ($editMode):
            ?>
            <div class="row">
                <div class="col s12 <?php echo $editMode ? "l6" : "" ?>">
                    <?php
                        endif;
                    ?>
                    <a class="left grey-text"><?php echo $username ?></a>
                    <a class="grey-text
                        <?php
                            if (isset($edit) && $edit){
                                echo "left";
                            }
                            else{
                                echo "right";
                            }
                        ?>
                    "><?php echo $timestamp ?></a>

                    <?php
                        if ($editMode):
                    ?>
                </div>
                <div class="col s12 l6">
                    <?php
                        $args = $row["post_id"].", ".$row["archived"];
                        echo '<a href="#deletePost" onclick="deleteModalSetup('.$args.')" class="right tooltipped modal-trigger red-text" data-position="right" data-tooltip="Delete post"><i class="material-icons">delete</i></a>';
                        echo '<a href="../helpers/edit_post.php?id='.$row["post_id"].'&action=edit" class="right tooltipped blue-text" data-position="bottom" data-tooltip="Edit post"><i class="material-icons">edit</i></a>';
                        if ($row["archived"]){
                            echo '<a href="../helpers/edit_post.php?id='.$row["post_id"].'&action=publish" class="right tooltipped green-text" data-position="left" data-tooltip="Publish post"><i class="material-icons">publish</i></a>';
                        }
                        else {
                            echo '<a href="../helpers/edit_post.php?id='.$row["post_id"].'&action=archive" class="right tooltipped orange-text" data-position="left" data-tooltip="Archive post"><i class="material-icons">archive</i></a>';
                        }
                    ?>
                </div>
            </div>
        <?php
            endif;
        ?>
        </div>
    </div>
</div>
