<?php
    class Database
    {

        // Set/Change login credentials in config/credentials.php

        private $connection;

        // Connect to database when a new instance is created
        public function __construct() {
            require("../config/credentials.php");
            if (isset($host, $dbname, $username, $password)) {
                $this->connection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password) or die("Failed to connect to database: Missing credentials! <br><a href='mailto:admin@1samuel3.com'>Report to webmaster</a>");
                $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } else {
                die("Failed to connect to database: Missing credentials! <br><a href='../pages/'>Back to homepage</a>");
            }
        }

        public function read(string $query, array $params = null, int $limit = null) {
            if (isset($limit)){
                $query = $query." LIMIT :limit";
            }
            $statement = $this->connection->prepare($query);
            $statement->setFetchMode(PDO::FETCH_ASSOC);
            if (isset($limit)){
                $statement->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
            }
            if (isset($params)) {
                $statement->execute($params);
            } else {
                $statement->execute();
            }
            return $statement->fetchAll();
        }

        public function write(string $query, array $params = null) {
            $statement = $this->connection->prepare($query);
            if (isset($params)) {
                $statement->execute($params);
            } else {
                $statement->execute();
            }
        }

        public function isAdmin($username) {
            $data = $this->read("SELECT admin FROM user WHERE username = '$username'");
            if (sizeof($data) > 1) {
                die("Multiple users found! <br><a href='../pages'>Back to homepage</a>");
            }
            foreach ($data as $user) {
                return $user["admin"];
            }
        }

        public function getUserByName($username) {
            $rows = $this->read("SELECT user_id FROM user WHERE username = '$username'");
            foreach ($rows as $row) {
                $id = $row["user_id"];
            }
            if (isset($id)) {
                return $this->getUserById($id);
            }
            die("Something went wrong!<br><a href='../pages/'>Back to homepage</a>");
        }

        public function getUserById($userId) {
            foreach ($this->read("SELECT * FROM user WHERE user_id = $userId") as $row) {
                $userId = $row["user_id"];
                $username = $row["username"];
                $firstname = $row["firstname"];
                $lastName = $row["last_name"];
                $email = $row["email"];
                $admin = $row["admin"];
                $active = $row["active"];
                $createDate = $row["created_at"];
                $hash = $row["user_hash"];
                $displayName = $row["display_name"];
            }
            if (isset($userId, $username, $firstname, $firstname, $lastName, $email, $admin, $active, $createDate, $hash, $displayName)) {
                return array(
                    "userId" => $userId,
                    "username" => $username,
                    "firstname" => $firstname,
                    "lastname" => $lastName,
                    "email" => $email,
                    "isAdmin" => $admin,
                    "isActive" => $active,
                    "createDate" => $createDate,
                    "hash" => $hash,
                    "displayName" => $displayName
                );
            }
            die("Something went wrong! <br><a href='../pages/'>Back to homepage</a>");
        }

        public function getFaqItemsBySearch($term) {
            if ($term == "") {
                return $this->getFaqItems();
            }
            return $this->read("SELECT * FROM faq WHERE description LIKE '%$term%'");
        }

        public function getFaqItems() {
            return $this->read("SELECT * FROM faq");
        }

        public function getPostById($id) {
            return $this->read("SELECT * FROM post WHERE post_id=$id");
        }

        function updateDisplayName(){
            $user = $this->getUserByName($_SESSION["username"]);

            $email = $user["email"];
            include("gravatar.php");
            $_SESSION["pfp"] = $gravatar;

            $_SESSION["displayName"] = "";
            if (isset($user["displayName"])){
                if ($user["displayName"] == "realName"){
                    if (isset($user["firstname"]) && isset($user["lastname"])){
                        $_SESSION["displayName"] = $user["firstname"]." ".$user["lastname"];
                    }
                }
                else if ($user["displayName"] == "username"){
                    $_SESSION["displayName"] = $user["username"];
                }
                else{
                    $_SESSION["displayName"] = $user["displayName"];
                }
            }
        }
    }